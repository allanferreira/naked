const express = require('express')
const app = express()

const port = process.env.port || 8080
const dir  = `${__dirname}/app`

app.use(express.static(dir))

app.get('/', (req, res) => res.sendFile(`${dir}/index.html`))
app.listen(port, () => console.log(`running in ${port}`))